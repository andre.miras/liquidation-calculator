import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Calculator from './components/Calculator';
import Header from './components/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="container">
        <Calculator />
      </div>
    </div>
  );
}

export default App;

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Card, Col, FormControl, InputGroup, Row,
} from 'react-bootstrap';
import { fetchPrices } from '../utils/compound-prices-api';

const PrependInput = ({
  prependText, value, onChange, type, disabled,
}) => (
  <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text>{prependText}</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl value={value} onChange={onChange} disabled={disabled} type={type} />
  </InputGroup>
);
PrependInput.propTypes = {
  prependText: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  type: PropTypes.string,
  disabled: PropTypes.bool,
};
PrependInput.defaultProps = {
  onChange: null,
  type: 'text',
  disabled: false,
};

const LabelInput = ({ labelText, inputElement }) => (
  <Row className="w-100">
    <Col>
      {labelText}
    </Col>
    <Col>
      {inputElement}
    </Col>
  </Row>
);
LabelInput.propTypes = {
  labelText: PropTypes.string.isRequired,
  inputElement: PropTypes.element.isRequired,
};

const EthPriceInput = ({ price, onPrice }) => (
  <LabelInput
    labelText="ETH price:"
    inputElement={(
      <PrependInput
        prependText="$"
        value={price.toString()}
        onChange={(e) => { onPrice(Number(e.target.value)); }}
        type="number"
      />
      )}
  />
);
EthPriceInput.propTypes = {
  price: PropTypes.number.isRequired,
  onPrice: PropTypes.func.isRequired,
};

const EthBalanceInput = ({ balance, onBalance }) => (
  <LabelInput
    labelText="ETH balance:"
    inputElement={(
      <PrependInput
        prependText="Ξ"
        value={balance.toString()}
        onChange={(e) => { onBalance(Number(e.target.value)); }}
        type="number"
      />
      )}
  />
);
EthBalanceInput.propTypes = {
  onBalance: PropTypes.func.isRequired,
  balance: PropTypes.number.isRequired,
};

const StablePriceInput = ({ price, onPrice }) => (
  <LabelInput
    labelText="Stable price:"
    inputElement={(
      <PrependInput
        prependText="$"
        value={price.toString()}
        onChange={(e) => { onPrice(Number(e.target.value)); }}
        type="number"
      />
      )}
  />
);
StablePriceInput.propTypes = {
  price: PropTypes.number.isRequired,
  onPrice: PropTypes.func.isRequired,
};

const BorrowedInput = ({ borrowed, onBorrowed }) => (
  <LabelInput
    labelText="Borrowed:"
    inputElement={(
      <PrependInput
        prependText="$"
        value={borrowed.toString()}
        onChange={(e) => { onBorrowed(Number(e.target.value)); }}
        type="number"
      />
      )}
  />
);
BorrowedInput.propTypes = {
  onBorrowed: PropTypes.func.isRequired,
  borrowed: PropTypes.number.isRequired,
};

const Supply = ({
  price, setPrice, balance, setBalance,
}) => (
  <Card>
    <Card.Header>Supply</Card.Header>
    <Card.Body>
      <EthPriceInput price={price} onPrice={setPrice} />
      <EthBalanceInput balance={balance} onBalance={setBalance} />
    </Card.Body>
  </Card>
);
Supply.propTypes = {
  price: PropTypes.number.isRequired,
  setPrice: PropTypes.func.isRequired,
  balance: PropTypes.number.isRequired,
  setBalance: PropTypes.func.isRequired,
};

const Borrow = ({
  price, setPrice, borrowed, setBorrowed,
}) => (
  <Card>
    <Card.Header>Borrow</Card.Header>
    <Card.Body>
      <StablePriceInput price={price} onPrice={setPrice} />
      <BorrowedInput borrowed={borrowed} onBorrowed={setBorrowed} />
    </Card.Body>
  </Card>
);
Borrow.propTypes = {
  price: PropTypes.number.isRequired,
  setPrice: PropTypes.func.isRequired,
  borrowed: PropTypes.number.isRequired,
  setBorrowed: PropTypes.func.isRequired,
};

const SupplyBorrow = ({
  supplyPrice,
  setSupplyPrice,
  supplyBalance,
  setSupplyBalance,
  borrowPrice,
  setBorrowPrice,
  borrowBalance,
  setBorrowBalance,
}) => (
  <Row className="mb-4">
    <Col>
      <Supply
        price={supplyPrice}
        setPrice={setSupplyPrice}
        balance={supplyBalance}
        setBalance={setSupplyBalance}
      />
    </Col>
    <Col>
      <Borrow
        price={borrowPrice}
        setPrice={setBorrowPrice}
        borrowed={borrowBalance}
        setBorrowed={setBorrowBalance}
      />
    </Col>
  </Row>
);
SupplyBorrow.propTypes = {
  supplyPrice: PropTypes.number.isRequired,
  setSupplyPrice: PropTypes.func.isRequired,
  supplyBalance: PropTypes.number.isRequired,
  setSupplyBalance: PropTypes.func.isRequired,
  borrowPrice: PropTypes.number.isRequired,
  setBorrowPrice: PropTypes.func.isRequired,
  borrowBalance: PropTypes.number.isRequired,
  setBorrowBalance: PropTypes.func.isRequired,
};

const MaxBorrow = ({ value }) => (
  <LabelInput
    labelText="Max borrow:"
    inputElement={(
      <PrependInput
        prependText="$"
        value={value.toFixed(2)}
        disabled
      />
      )}
  />
);
MaxBorrow.propTypes = {
  value: PropTypes.number.isRequired,
};

const LiquidationPrice = ({ price }) => (
  <LabelInput
    labelText="Liquidation price:"
    inputElement={(
      <PrependInput
        prependText="$"
        value={price.toFixed(2)}
        disabled
      />
      )}
  />
);
LiquidationPrice.propTypes = {
  price: PropTypes.number.isRequired,
};

const Calculator = () => {
  const [supplyPrice, setSupplyPrice] = useState(1000);
  const [supplyBalance, setSupplyBalance] = useState(1);
  const [borrowPrice, setBorrowPrice] = useState(1);
  const [borrowBalance, setBorrowBalance] = useState(100);
  const collateralFactor = 0.75;
  const maxBorrow = collateralFactor * supplyPrice * supplyBalance;
  const liquidationPrice = supplyPrice * ((borrowPrice * borrowBalance) / maxBorrow);
  const onOk = (data) => {
    setSupplyPrice(Number(data.prices.ETH).toFixed(2));
  };
  const onNotOk = (data) => {
    // TODO: error handling, show a message UI side saying the price feed isn't available
    /* eslint-disable no-console */
    console.error(data);
  };
  useEffect(() => {
    fetchPrices(onOk, onNotOk);
  }, []);
  return (
    <>
      <SupplyBorrow
        supplyPrice={supplyPrice}
        setSupplyPrice={setSupplyPrice}
        supplyBalance={supplyBalance}
        setSupplyBalance={setSupplyBalance}
        borrowPrice={borrowPrice}
        setBorrowPrice={setBorrowPrice}
        borrowBalance={borrowBalance}
        setBorrowBalance={setBorrowBalance}
      />
      <MaxBorrow value={maxBorrow} />
      <LiquidationPrice price={liquidationPrice} />
    </>
  );
};

export default Calculator;

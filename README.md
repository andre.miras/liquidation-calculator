# Liquidation Calculator

Given a portfolio find the liquidation price.

- https://andre.miras.gitlab.io/liquidation-calculator/ (broken SSL)
- http://andre.miras.gitlab.io/liquidation-calculator/ (no SSL)

## Roadmap
- [x] Basic calculator with single asset
- [ ] Multi asset
- [ ] Retrieve portfolio given an address (compound.finance only)
- [ ] Support more plateforms (MakerDAO, Aave)


## Run
```sh
yarn start
```

## Test
```sh
yarn lint
yarn test
```
